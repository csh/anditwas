pdf:
	noweave anditwas.nw > anditwas.tex; pdflatex anditwas.tex
bin: 
	notangle anditwas.nw > anditwas
clean:
	rm -rf *.aux *.log *.tex
